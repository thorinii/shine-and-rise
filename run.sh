#!/usr/bin/env bash
set -e

base_dir="$(cd "$(dirname "$0")"; pwd)"
prog="${1:-main.py}"

. "${base_dir}/venv/bin/activate"

python "${base_dir}/${prog}"
