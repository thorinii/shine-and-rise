#!/usr/bin/env python3

import adafruit_ws2801
import board
import datetime
import time

DEBUG_MODE = False

T0_HOUR = 4
T0_MINUTE = 50

T0_HOUR -= 9
T0_MINUTE -= 30
if T0_MINUTE < 0:
    T0_MINUTE += 60
    T0_HOUR -= 1
if T0_HOUR < 0:
    T0_HOUR += 24
T_END_TIME = datetime.timedelta(minutes=2 if DEBUG_MODE else 30)
STEP_INTERVAL_SECONDS = datetime.timedelta(seconds=5)
TIME_TILL_OFF = datetime.timedelta(hours=2)
ALPHA_OFFSET_BLOCK = 18
ALPHA_OFFSET_TARGET = 0.5

PIN_CLOCK = board.SCLK
PIN_DATA = board.MOSI
ARRAY_LENGTH = 32 * 5
MAX_LEDS_ON = 32 * 4 - 10 # LEDs visible
MAX_LEDS_ON = min(ARRAY_LENGTH, MAX_LEDS_ON)

LED_ARRAY = adafruit_ws2801.WS2801(PIN_CLOCK, PIN_DATA, ARRAY_LENGTH)
LED_ARRAY.auto_write = False

def set_all(color):
    color = (int(color[0] * 255), int(color[1] * 255), int(color[2] * 255))
    for i in range(0, MAX_LEDS_ON):
        LED_ARRAY[i] = color
    LED_ARRAY.show()


def set_all_alpha(alpha):
    for i in range(0, MAX_LEDS_ON):
        led_index = int(i % ALPHA_OFFSET_BLOCK)
        alpha_offset = ALPHA_OFFSET_TARGET * float(led_index) / ALPHA_OFFSET_BLOCK
        LED_ARRAY[i] = color_int(color(alpha - alpha_offset))
    LED_ARRAY.show()


def color(alpha):
    red = alpha
    green = (alpha - 0.18) / 0.82
    blue = (alpha - 0.7) / 0.3
    return (blue, green, red)

def color_int(color):
    return (
            max(0, min(255, int(color[0] * 255))),
            max(0, min(255, int(color[1] * 255))),
            max(0, min(255, int(color[2] * 255))))


# calculate
startup_time = datetime.datetime.now()
t0 = startup_time.replace(hour=T0_HOUR, minute=T0_MINUTE)
if t0 < startup_time:
    t0 = t0 + datetime.timedelta(days=1)
if DEBUG_MODE:
    t0 = startup_time
t_end = t0 + T_END_TIME

time_till_t0 = (t0 - startup_time).total_seconds()


# setup
set_all((0, 0, 0))


try:
    # wait till t0
    if time_till_t0 > 0:
        print('waiting', time_till_t0, 'seconds till t0:', t0)
        time.sleep(time_till_t0)


    # run loop
    now = datetime.datetime.now()
    while now < t_end:
        spent = now - t0
        alpha = 1.0 * (spent).total_seconds() / T_END_TIME.total_seconds()
        alpha = alpha * alpha
        alpha = max(0, min(1, alpha))
        print('alpha:', alpha)

        set_all_alpha(alpha)

        time.sleep(STEP_INTERVAL_SECONDS.total_seconds())
        now = datetime.datetime.now()


    # stay on maximum
    set_all((1, 1, 1))

    print('staying on for', TIME_TILL_OFF.total_seconds(), 'seconds')
    time.sleep(TIME_TILL_OFF.total_seconds())
except Exception as e:
    print('error', e)
except:
    pass

print('resetting to off')
set_all((0, 0, 0))
