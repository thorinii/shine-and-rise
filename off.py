#!/usr/bin/env python3

import adafruit_ws2801
import board
import datetime
import time

PIN_CLOCK = board.SCLK
PIN_DATA = board.MOSI
ARRAY_LENGTH = 32 * 5

LED_ARRAY = adafruit_ws2801.WS2801(PIN_CLOCK, PIN_DATA, ARRAY_LENGTH)
LED_ARRAY._spi.configure(baudrate=1000 * 1000, phase=0)

print('turning off')
LED_ARRAY.fill((0, 0, 0))
